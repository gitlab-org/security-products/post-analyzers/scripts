# Post-Analyzer Start changelog

## v0.4.0
- Remove VET as a post-analyzer (!13)

## v0.3.0
- Adds support to choose Secret Detection report as the input to Tracking Calculator (!12)

## v0.2.0
- Enable Go VET FP reduction for all project namespaces (!11)

## v0.1.0
- Add timeout of 5 minutes to run VET operation (!10)
- Restrict VET FP reduction only to Ruby for non-gitlab projects (!10)

## v0.0.7
- Accept language import filter via `VET_LANG_EXT` env var (!8)

## v0.0.6
- Modify VET script (!7)

## v0.0.5
- Bump VET to enable specifying output file, rather than directory (!6)

## v0.0.4
- Run VET regardless of TC outcome (!5)

## v0.0.3
- Return non-zero exit codes from analyzer-binary (!4)

## v0.0.2
- Hardcode stencils dir

## v0.0.1
- VET and TC start

