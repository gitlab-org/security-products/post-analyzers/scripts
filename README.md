# scripts

Collection of shell scripts packaged with SAST analyzers to enable post-analyzer
integrations.

## Tests

Test cases are executed using [`bats`](https://github.com/bats-core/bats-core).

```
❯ docker run -it -v "$PWD:/code" bats/bats:latest start.bats
 ✓ returns zero exit-code for analyzer-binary
 ✓ returns non-zero exit-codes for analyzer-binary

2 tests, 0 failures

```
