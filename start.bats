#!/usr/bin/env bats

ANALYZER_BINARY_PATH="${BATS_TEST_DIRNAME}/analyzer-binary"
ANALYZER_TRACKING_BINARY_PATH="${BATS_TEST_DIRNAME}/analyzer-tracking"
SAST_REPORT_PATH="$BATS_TEST_DIRNAME/gl-sast-report.json"
SD_REPORT_PATH="$BATS_TEST_DIRNAME/gl-secret-detection-report.json"

OUTPUT_PATH="$BATS_TEST_DIRNAME/gl-report-post.json"

# Creates a dummy binary executable for given
# file name($1) and exits with ($2) status code
#
# Params:
# $1: Name of the binary file
# $2: Exit status code of the binary
setup_binary_with_exit_code() {
  touch $1
  chmod +x $1

  echo "#!/bin/sh" >$1
  echo "exit $2 " >>$1
}

# Creates dummy binary executable  for given file name($1)
# which appends a sleep command of ($2) seconds and finally
# exits with ($3) code
#
# Params:
# $1: Name of the binary file
# $2: No of seconds the binary executable should sleep
# $3: Exit status code of the binary
setup_delayed_binary_with_exit_code() {
  touch $1
  chmod +x $1

  echo "#!/bin/sh" >$1
  echo "sleep $2" >>$1
  echo "exit $3 " >>$1
}

setup_file() {
  export CI_PROJECT_DIR=$BATS_TEST_DIRNAME
  export SCRIPT_BASE_DIR=$BATS_TEST_DIRNAME
}

teardown_file() {
  [ "$GITLAB_FEATURES" = "" ] || unset GITLAB_FEATURES
  [ "$SCRIPT_BASE_DIR" = "" ] || unset SCRIPT_BASE_DIR
}

teardown() {
  rm -f $SAST_REPORT_PATH
  rm -f $OUTPUT_PATH
  rm -f $ANALYZER_BINARY_PATH
  rm -f $ANALYZER_TRACKING_BINARY_PATH
}

@test "returns zero exit-code for analyzer-binary" {
  setup_binary_with_exit_code $ANALYZER_BINARY_PATH 0
  run ./start.sh
  [ "$status" -eq 0 ]
  [ "$output" = "" ]
}

@test "returns non-zero exit-codes for analyzer-binary" {
  setup_binary_with_exit_code $ANALYZER_BINARY_PATH 1
  run ./start.sh
  [ "$status" -eq 1 ]
  [ "$output" = "" ]

  teardown

  setup_binary_with_exit_code $ANALYZER_BINARY_PATH 127
  run ./start.sh
  [ "$status" -eq 127 ]
  [ "$output" = "" ]
}

@test "returns zero exit-code for tracking failure" {
  setup_binary_with_exit_code $ANALYZER_BINARY_PATH 0
  setup_binary_with_exit_code $ANALYZER_TRACKING_BINARY_PATH 127
  export GITLAB_FEATURES="vulnerability_finding_signatures"

  touch $SAST_REPORT_PATH

  run ./start.sh
  [ "$status" -eq 0 ]
  [ "$output" = "" ]
}

@test "returns zero exit-code when analyzer-tracking is not present" {
  setup_binary_with_exit_code $ANALYZER_BINARY_PATH 0
  export GITLAB_FEATURES="vulnerability_finding_signatures"

  touch $SAST_REPORT_PATH
  cp $SAST_REPORT_PATH $OUTPUT_PATH

  run ./start.sh
  [ "$status" -eq 0 ]
  [ "$output" = "" ]
}

@test "chooses SAST report as input when \$REPORT_TYPE CI var is set to 'sast'" {
  setup_binary_with_exit_code $ANALYZER_BINARY_PATH 0
  setup_binary_with_exit_code $ANALYZER_TRACKING_BINARY_PATH 0
  export REPORT_TYPE="sast"
  export SECURE_LOG_LEVEL="debug"

  run ./start.sh
  [ "$status" -eq 0 ]
  [[ "$output" == *"[DEBUG] ▶ Choosing the input analyzer report: '$SAST_REPORT_PATH'"* ]]
}

@test "chooses Secret Detection report as input when \$REPORT_TYPE env var is set to 'secret-detection'" {
  setup_binary_with_exit_code $ANALYZER_BINARY_PATH 0
  setup_binary_with_exit_code $ANALYZER_TRACKING_BINARY_PATH 0
  export REPORT_TYPE="secret-detection"
  export SECURE_LOG_LEVEL="debug"

  run ./start.sh
  [ "$status" -eq 0 ]
  [[ "$output" == *"[DEBUG] ▶ Choosing the input analyzer report: '$SD_REPORT_PATH'"* ]]
}

@test "chooses SAST report as input by default when \$REPORT_TYPE env var is undefined or incorrect" {
  setup_binary_with_exit_code $ANALYZER_BINARY_PATH 0
  setup_binary_with_exit_code $ANALYZER_TRACKING_BINARY_PATH 0
  export REPORT_TYPE=""
  export SECURE_LOG_LEVEL="debug"

  run ./start.sh

  [ "$status" -eq 0 ]
  [[ "$output" == *"[DEBUG] ▶ Choosing the input analyzer report: '$SAST_REPORT_PATH'"* ]]
}
